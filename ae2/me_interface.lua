me_interface = {}

function me_interface:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    return o
end

function me_interface:listItems()
    local comp = require("component")
    local iface = comp.me_interface
    local itemlist = iface.getItemsInNetwork()
    for i,v in ipairs(itemlist) do
        print(v.name .. " " .. v.size)
    end
end