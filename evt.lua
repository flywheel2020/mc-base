local evtKeyUp = 0
local evtKeyDown = 0
local tmrTimerId = 0
local clk = 0.0
local inspect = require("inspect")
local event = require("event")
local fs = require("filesystem")
local quit = false
local f = fs.open("clock.txt", "wb")
function eventHandler(eventId, ...)
    local arg = {...}
    --print(inspect(arg) .. clk)
    f:write(clk .. " " .. os.clock() .. "\n")
    if eventId == "key_up" and arg[3] == 16 then quit = true end
end

evtKeyUp = event.listen("key_up", eventHandler)
evtKeyDown = event.listen("key_down", eventHandler)
tmrTimerId = event.timer(0.05, function() clk = clk + 0.05 end, math.huge)

while not quit do
    os.sleep(0.01)
end

event.ignore("key_up", eventHandler)
event.ignore("key_down", eventHandler)
event.cancel(tmrTimerId)
f:close()