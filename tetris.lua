local BLK1 = {"▗", "▖", "▝", "▘"}
local SOLID = "█"
local RNDBOX = {"╭", "╮", "╯", "╰", "─", "│"}
local EDGEBOX = {"▔", "▕", "▁", "▏"}
local LATERAL_REPEAT = 0.15
local FALL_REPEAT = 0.10
local comp = require("component")
--local cpu = require("computer")
local event = require("event")
--local inspect = require("inspect")
local os = require("os")
local gpu = comp.gpu
local w,h = gpu.getResolution()
local playfield = {}
local gameClock = 0.0
local level = 0
local score = 0
local lines = 0
local fallSpeed = 0.6
local currentPiece = nil
local nextPiece = {}
local lastFallDown = 0.0
local lastMoveDown = 0.0
local lastMoveLateral = 0.0
local movingLeft = false
local movingRight = false
local movingDown = false
local loopRunning = false
local processing = false
local tmrId

local O_Piece = {{ '.....',
                   '.....',
                   '..OO.',
                   '..OO.',
                   '.....'}}

local S_Piece = {{ '.....',
                   '.....',
                   '..OO.',
                   '.OO..',
                   '.....'},
                  {'.....',
                   '..O..',
                   '..OO.',
                   '...O.',
                   '.....'}}

local Z_Piece = {{ '.....',
                   '.....',
                   '.OO..',
                   '..OO.',
                   '.....'},
                  {'.....',
                   '...O.',
                   '..OO.',
                   '..O..',
                   '.....'}}

local I_Piece = {{ '.....',
                   '..O..',
                   '..O..',
                   '..O..',
                   '..O..'},
                  {'.....',
                   '.....',
                   'OOOO.',
                   '.....',
                   '.....'}}

local J_Piece = {{ '.....',
                   '.O...',
                   '.OOO.',
                   '.....',
                   '.....'},
                  {'.....',
                   '.OO..',
                   '.O...',
                   '.O...',
                   '.....'},
                   {'.....',
                    '.OOO.',
                    '...O.',
                    '.....',
                    '.....'},
                    {'.....',
                     '..O..',
                     '..O..',
                     '.OO..',
                     '.....'}}

local L_Piece = {{ '.....',
                   '...O.',
                   '.OOO.',
                   '.....',
                   '.....'},
                  {'.....',
                   '.O...',
                   '.O...',
                   '.OO..',
                   '.....'},
                   {'.....',
                    '.OOO.',
                    '.O...',
                    '.....',
                    '.....'},
                    {'.....',
                     '.OO..',
                     '..O..',
                     '..O..',
                     '.....'}}

local T_Piece = {{ '.....',
                   '..O..',
                   '.OOO.',
                   '.....',
                   '.....'},
                  {'.....',
                   '.O...',
                   '.OO..',
                   '.O...',
                   '.....'},
                   {'.....',
                    '.OOO.',
                    '..O..',
                    '.....',
                    '.....'},
                    {'.....',
                     '..O..',
                     '.OO..',
                     '..O..',
                     '.....'}}




--
local Tetrominoes = { O_Piece, S_Piece, Z_Piece, I_Piece, J_Piece, L_Piece, T_Piece }

local PieceColors = {
    {0xff6d00, 0xff6d00},
    {0x006dff, 0x006dff},
    {0x006d00, 0x006d00},
    {0xff6dff, 0xff6dff},
    {0xff0000, 0xff0000},
    {0xffff00, 0xffff00},
    {0x9949c0, 0x9949c0}
}
--
local prepareTetrominoes = function ()
    for tetromino = 1,#Tetrominoes do
        for rotate = 1,#Tetrominoes[tetromino] do
            for y = 1,#Tetrominoes[tetromino][rotate] do
                local nl = {}
                for x = 1,string.len(Tetrominoes[tetromino][rotate][y]) do
                    table.insert(nl, string.sub(Tetrominoes[tetromino][rotate][y], x, x))
                end
                Tetrominoes[tetromino][rotate][y] = nl
            end
        end
    end
end

local clearScreen = function()
    gpu.setBackground(0x000000)
    gpu.fill(1, 1, w, h, " ")
end

local fillPlay = function (color)
    gpu.setForeground(color)
    gpu.fill(61, 4, 100, 44, SOLID)
end

local blocklet = function (x, y)
    x = x - 1
    y = y - 1
    gpu.set(61 + (4 * x), 4 + (2 * y), BLK1[3] .. BLK1[4] .. BLK1[4] .. BLK1[4])
    gpu.set(61 + (4 * x), 5 + (2 * y), BLK1[1] .. BLK1[2] .. BLK1[2] .. BLK1[2])
end

local initPlayfield = function ()
    playfield = {}
    repeat
        table.insert(playfield, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0})
    until #playfield == 20
end

local paintNextPieceFrame = function ()
    gpu.setForeground(0xFFFFFF)
    gpu.setBackground(0x000000)
    -- 121, 30
    gpu.set(119, 33, RNDBOX[1] .. string.rep(RNDBOX[5], 7) .. " NEXT " .. string.rep(RNDBOX[5], 7) .. RNDBOX[2])
    for i = 1,10 do
        gpu.set(119,33 + i, RNDBOX[6] .. string.rep(" ", 20) .. RNDBOX[6])
    end
    gpu.set(119, 44, RNDBOX[4] .. string.rep(RNDBOX[5], 20) .. RNDBOX[3])

    gpu.setForeground(nextPiece.color[1])
    gpu.setBackground(nextPiece.color[2])
    for y = 1,5 do
        for x = 1,5 do
            if Tetrominoes[nextPiece.shape][1][y][x] ~= "." then
                gpu.set(116 + (4 * x), 32 + (2 * y), BLK1[3] .. BLK1[4] .. BLK1[4] .. BLK1[4])
                gpu.set(116 + (4 * x), 33 + (2 * y), BLK1[1] .. BLK1[2] .. BLK1[2] .. BLK1[2])
            end
        end
    end
end

local getLevelSpeed = function ()
    level = math.floor(lines/10)
    if level < 30 then
        fallSpeed = 0.6 - (level * 0.02)
    else
        fallSpeed = 0.02
    end
    return level, fallSpeed
end

--[[--
local reseedRNG = function ()
    math.randomseed(gameClock + os.clock())
end
--]]

local grabNewPiece = function ()
    --reseedRNG()
    local partIndex = math.random(#Tetrominoes)
    local newPiece = {}
    newPiece["shape"] = partIndex
    newPiece["rotation"] = 0
    newPiece["x"] = 3
    newPiece["y"] = -2
    newPiece["color"] = PieceColors[partIndex]
    return newPiece
end

local checkCoordOnPlayfield = function (x, y)
    if x > 0 and x < 11 and y < 21 then return true end
    return false
end

local checkRestingCoord = function (x, y)
    if x > 0 and x < 11 and y > 0 and y < 21 then return true end
    return false
end

local validatePiecePosition = function (tpiece, movX, movY)
    local pX, pY = tpiece.x - 1, tpiece.y - 1
    for y = 1,5 do
        for x = 1,5 do
            repeat
                if (y + pY + movY) < 1 then do break end end
                if Tetrominoes[tpiece.shape][tpiece.rotation+1][y][x] == "." then do break end end
                if checkCoordOnPlayfield(x + pX + movX, y + pY + movY) == false then return false end
                if playfield[y + pY + movY][x + pX + movX] ~= 0 then return false end
            until true
        end
    end
    return true
end

local checkLineComplete = function (y)
    for x = 1,#playfield[y] do
        if playfield[y][x] == 0 then return false end
    end
    return true
end

local removeCompletedLines = function ()
    local linesRemoved = 0
    local newPlayfield = {}
    local blankLine = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
    for y = 20,1,-1 do
        if checkLineComplete(y) == true then
            linesRemoved = linesRemoved + 1
        else
            table.insert(newPlayfield, 1, playfield[y])
        end
    end
    if linesRemoved > 0 then
        repeat
            table.insert(newPlayfield, 1, blankLine)
        until #newPlayfield == 20
        playfield = newPlayfield
    end
    return linesRemoved
end

local transferToPlayfield = function (tpiece)
    local pX, pY = tpiece.x - 1, tpiece.y - 1
    for y = 1,5 do
        for x = 1,5 do
            if Tetrominoes[tpiece.shape][tpiece.rotation+1][y][x] ~= "." then
                if not checkRestingCoord(x + pX, y + pY) then return false end
                playfield[y + pY][x + pX] = tpiece.color
            end
        end
    end
    return true
end

local paintPlayfield = function ()
    fillPlay(0)
    gpu.setForeground(0xFFFFFF)
    gpu.setBackground(0x000000)
    gpu.fill(60, 4, 1, 40, EDGEBOX[2])
    gpu.fill(61, 44, 40, 1, EDGEBOX[1])
    gpu.fill(101, 4, 1, 40, EDGEBOX[4])
    for y = 1,20 do
        for x = 1,10 do
            if playfield[y][x] ~= 0 then
                gpu.setForeground(playfield[y][x][1])
                gpu.setBackground(playfield[y][x][2])
                blocklet(x, y)
            end
        end
    end
end

local paintStats = function ()
    gpu.setForeground(0xFFFFFF)
    gpu.setBackground(0x000000)
    gpu.set(114, 5, "Score: " .. score)
    gpu.set(114, 6, "Level: " .. level)
    gpu.set(114, 7, "Lines: " .. lines)
end

local paintCurrentPiece = function (color1, color2)
    gpu.setForeground(color1)
    gpu.setBackground(color2)
    for y = 1,5 do
        for x = 1,5 do
            if Tetrominoes[currentPiece.shape][currentPiece.rotation+1][y][x] ~= "." then
                blocklet(currentPiece["x"] + x - 1, currentPiece["y"] + y - 1)
            end
        end
    end
end


--[[--
    200 UP ARROW
    205 RIGHT ARROW
    208 DOWN ARROW
    203 LEFT ARROW
     57 SPACE
--]]--
local eventHandler = function (eventId, ...)
    local arg = {...}

    if eventId == "key_up" and arg[3] == 203 then
        movingLeft = false
    elseif eventId == "key_up" and arg[3] == 205 then
        movingRight = false
    elseif eventId == "key_up" and arg[3] == 208 then
        movingDown = false
    elseif eventId == "key_down" and arg[3] == 203 and not movingLeft and not processing  then
        processing = true
        if validatePiecePosition(currentPiece, -1, 0) then
            paintCurrentPiece(0, 0)
            currentPiece.x = currentPiece.x - 1
            paintCurrentPiece(currentPiece.color[1], currentPiece.color[2])
            movingLeft = true
            movingRight = false
            lastMoveLateral = gameClock
        end
        processing = false
    elseif eventId == "key_down" and arg[3] == 205 and not movingRight and not processing then
        processing = true
        if validatePiecePosition(currentPiece, 1, 0) then
            paintCurrentPiece(0, 0)
            currentPiece.x = currentPiece.x + 1
            paintCurrentPiece(currentPiece.color[1], currentPiece.color[2])
            movingLeft = false
            movingRight = true
            lastMoveLateral = gameClock
        end
        processing = false
    elseif eventId == "key_down" and arg[3] == 200 and not processing then
        processing = true
        paintCurrentPiece(0, 0)
        currentPiece.rotation = math.fmod((currentPiece.rotation + 1), #Tetrominoes[currentPiece.shape])
        if not validatePiecePosition(currentPiece, 0, 0) then
            currentPiece.rotation = math.fmod((currentPiece.rotation - 1), #Tetrominoes[currentPiece.shape])
        end
        paintCurrentPiece(currentPiece.color[1], currentPiece.color[2])
        processing = false
    elseif eventId == "key_down" and arg[3] == 208 and not movingDown and not processing then
        processing = true
        if validatePiecePosition(currentPiece, 0, 1) then
            score = score + 1
            paintStats()
            paintCurrentPiece(0, 0)
            currentPiece.y = currentPiece.y + 1
            paintCurrentPiece(currentPiece.color[1], currentPiece.color[2])
            movingDown = true
            lastMoveDown = gameClock
        end
        processing = false
    end
end

local playGame = function ()
    initPlayfield()
    lastMoveDown = gameClock
    lastMoveLateral = gameClock
    lastFallDown = gameClock
    movingDown = false
    movingLeft = false
    movingRight = false
    score = 0
    lines = 0
    level, fallSpeed = getLevelSpeed(lines)
    currentPiece = grabNewPiece()
    nextPiece = grabNewPiece()
    paintPlayfield()
    paintNextPieceFrame()
    paintStats()

    loopRunning = true

    while loopRunning do
        if currentPiece == nil then
            currentPiece = nextPiece
            nextPiece = grabNewPiece()
            paintNextPieceFrame()
            if not validatePiecePosition(currentPiece, 0, 0) then return end
        end

        if (movingLeft or movingRight) and gameClock - lastMoveLateral > LATERAL_REPEAT and not processing then
            processing = true
            if movingLeft and validatePiecePosition(currentPiece, -1, 0) then
                paintCurrentPiece(0, 0)
                currentPiece.x = currentPiece.x - 1
                paintCurrentPiece(currentPiece.color[1], currentPiece.color[2])
            elseif movingRight and validatePiecePosition(currentPiece, 1, 0) then
                paintCurrentPiece(0, 0)
                currentPiece.x = currentPiece.x + 1
                paintCurrentPiece(currentPiece.color[1], currentPiece.color[2])
            end
            lastMoveLateral = gameClock
            processing = false
        end
        if movingDown and gameClock - lastMoveDown > FALL_REPEAT and validatePiecePosition(currentPiece, 0, 1) and
        not processing then
            processing = true
            score = score + 1
            paintStats()
            paintCurrentPiece(0, 0)
            currentPiece.y = currentPiece.y + 1
            paintCurrentPiece(currentPiece.color[1], currentPiece.color[2])
            lastMoveDown = gameClock
            lastFallDown = gameClock
            processing = false
        end

        if gameClock - lastFallDown > fallSpeed and not processing then
            processing = true
            if not validatePiecePosition(currentPiece, 0, 1) then
                if not transferToPlayfield(currentPiece) then
                    --game over
                    loopRunning = false
                else
                    local newLines = removeCompletedLines()
                    if newLines == 1 then
                        score = score + (40 * (level + 1))
                    elseif newLines == 2 then
                        score = score + (100 * (level + 1))
                    elseif newLines == 3 then
                        score = score + (300 * (level + 1))
                    elseif newLines == 4 then
                        score = score + (1200 * (level + 1))
                    end
                    if newLines > 0 then
                        lines = lines + newLines
                        level, fallSpeed = getLevelSpeed(lines)
                        paintPlayfield()
                        paintStats()
                    end
                    currentPiece = nil
                end
            else
                paintCurrentPiece(0, 0)
                currentPiece.y = currentPiece.y + 1
                paintCurrentPiece(currentPiece.color[1], currentPiece.color[2])
            end
            lastFallDown = gameClock
            processing = false
        end

        os.sleep(0.01)
    end
end


clearScreen()

tmrId = event.timer(0.05, function () gameClock = gameClock + 0.05 end, math.huge)
event.listen("key_up", eventHandler)
event.listen("key_down", eventHandler)
prepareTetrominoes()

fillPlay(0)
playGame()

event.cancel(tmrId)
event.ignore("key_up", eventHandler)
event.ignore("key_down", eventHandler)